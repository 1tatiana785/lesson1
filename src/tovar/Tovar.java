package tovar;

import java.util.HashMap;
import java.util.Scanner;

public class Tovar {
    public static void main(String args[]) {
        price();
    }

    public static void price() {
        HashMap<String, Double> tovar = new HashMap();
        tovar.put("2ф", 5.7);
        tovar.put("Код 1", 10.0);
        tovar.put("Код мандаринов", 7.9);

        Scanner scan = new Scanner(System.in);
        System.out.println("Введите код :");
        String x = scan.next();
        Double priceProduct = tovar.get(x);
        if (priceProduct != null) {
            System.out.println("Цена: " + priceProduct);
        } else System.out.println("Код не найден");

        price();
    }

}
